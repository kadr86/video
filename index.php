<?php

class MakeVideo
{
    public $userFiles;
    public $uploadPath;
    public $dirToSaveFinishVideo;
    public $scaleX;
    public $scaleY;
    public $position;

    const UPLOAD_DIR = '/upload/';
    const FINISH_DIR = '/finish/';
    const BASE_VIDEO_SIZE = '640x480';
    const SCALE = '320x240';
    const PTS = 'PTS-STARTPTS';
    const CODEC = 'libx264';



    public function __construct()
    {
        $this->uploadPath = $_SERVER['DOCUMENT_ROOT'].self::UPLOAD_DIR;
        $this->dirToSaveFinishVideo = $_SERVER['DOCUMENT_ROOT'].self::FINISH_DIR;
        /**Создаем папку, если её нет, для загрузки туда присланных видео*/
        if (!is_dir($this->uploadPath)) mkdir($this->uploadPath);
        /**Создаем папку, если её нет, для сохранения туда результата работы скрипта*/
        if (!is_dir($this->dirToSaveFinishVideo)) mkdir($this->dirToSaveFinishVideo);

        $scale = explode('x', self::SCALE);

        $this->scaleX = 'x='.$scale[0];
        $this->scaleY = 'y='.$scale[1];
    }

    /**
     * Выполняем обработку (склейку) видео
     * return string
     */
    public function execute()
    {
        /**Загружаем видео*/
        $this->uploadFiles();
        /**Считаем количество видосов*/
        $filesCount = count($this->getUserFiles());

        $filesPaths = '';
        $filesParams = '';
        $tmp = '';

        foreach ($this->getUserFiles() as $key => $path) {
            $filesPaths .= "-i $path ";
            $filesParams .= "[$key:v] setpts=".self::PTS.", scale=".self::SCALE." [".$this->getPosition($key)."];";
        }

        /**Так как первое видео является базовым и будет всегда вверху слева, то его не надо брать в расчет, удаляем его из массива*/
        $userFilesWithoutFirstVideo = $this->getUserFiles();
        unset($userFilesWithoutFirstVideo[0]);

        foreach ($userFilesWithoutFirstVideo as $key => $path)
        {
            if ($filesCount == $key)
                $tmp .= " [tmp$key][".$this->getPosition($key)."] overlay=shortest=1:".$this->scaleX.':'.$this->scaleY.''.(($filesCount > 2) ? " [tmp". ($key + 1) ."];" : ';');
            else
                $tmp .= " [tmp$key][".$this->getPosition($key)."] overlay=".$this->getOverlay($key).''.(($filesCount > 2) ? " [tmp". ($key + 1) ."]" : '');

        }
        $filter = "nullsrc=size=".self::BASE_VIDEO_SIZE." [base];
            $filesParams
            [base][upperleft] overlay=shortest=1".(count($userFilesWithoutFirstVideo) > 0 ? " [tmp1];" : ';').
            "$tmp";

        $outputFileName = time().'_output.mkv';
        $outputFilePath = $this->dirToSaveFinishVideo.$outputFileName;

        /**Выполняем консольный скрипт*/
        exec("ffmpeg $filesPaths -filter_complex '$filter' -c:v ".self::CODEC.' '.$outputFilePath);

        $this->setUserFiles($outputFilePath);

        return $_SERVER['HTTP_HOST'].self::FINISH_DIR.$outputFileName;
        /**Удаляем пользовательские файлы*/
//        $this->deleteUploadedFiles();

    }

    /**
     * Загружаем файлы на сервер
     */
    public function uploadFiles()
    {
        if (isset($_FILES['files']))
        {
            $files = $_FILES['files'];
            foreach ($files['name'] as $key => $file)
            {
                /**Если ошибка, пропускаем этот файл*/
                if ($files['error'][$key] != 0) continue; //throw new Exception('Some error for uploaded file');

                move_uploaded_file($files['tmp_name'][$key], $this->uploadPath.$file);

                /**Сохраняем в массив файлы с которыми будем работать, для дальнейшего удаление
                 только тех файлов, с которымы работали*/
                $this->setUserFiles($this->uploadPath.$file);
            }
        }
    }

    /**
     * @param $filePath
     * Записываем в массив файлы
     */
    public function setUserFiles($filePath)
    {
        $this->userFiles[] = $filePath;
    }

    /**
     * @return mixed
     */
    public function getUserFiles()
    {
        return $this->userFiles;
    }


    /**
     * Удаляем файлы пользователя
     */
    public function deleteUploadedFiles()
    {
        foreach ($this->getUserFiles() as $userFile) {
            unset($userFile);
        }
    }

    /**
     * Выдаем позицию видео, в зависимости от индекса
     * @param $key
     * @return string
     */
    public function getPosition($index)
    {
        $positions = [
            0 => 'upperleft',
            1 => 'upperright',
            2 => 'lowerleft',
            3 => 'lowerright',
        ];
        /**Если в массиве нет такого ключа, то ориентируемся на четность ключа*/
        if (!array_key_exists($index,$positions))
        {
            if ($index % 2 == 0)
                return 'lowerleft';
            else
                return 'lowerright';
        }
        return $positions[$index];
    }

    /**
     * Отдаем оверлей, в зависимости от индекса
     * @param $index
     * @return string
     */
    public function getOverlay($index)
    {
        if ($index % 2 == 0)
            return 'shortest=1:'.$this->scaleX;
        else
            return 'shortest=1:'.$this->scaleY;
    }
}


echo (new MakeVideo)->execute();
?>

